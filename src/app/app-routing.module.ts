import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
    {
        path: "apps",
        loadChildren: "./main/apps/apps.module#AppsModule",
    },
    {
        path: "mains",
        loadChildren: "./components/main.module#MainModule",
    },
    {
        path: "pages",
        loadChildren: "./main/pages/pages.module#PagesModule",
    },
    {
        path: "ui",
        loadChildren: "./main/ui/ui.module#UIModule",
    },
    {
        path: "documentation",
        loadChildren:
            "./main/documentation/documentation.module#DocumentationModule",
    },
    {
        path: "angular-material-elements",
        loadChildren:
            "./main/angular-material-elements/angular-material-elements.module#AngularMaterialElementsModule",
    },
    {
        path: "**",
        redirectTo: "apps/dashboards/analytics",
    },
];

@NgModule({
    imports: [CommonModule, [RouterModule.forRoot(routes)]],
    declarations: [],
})
export class AppRoutingModule {}
